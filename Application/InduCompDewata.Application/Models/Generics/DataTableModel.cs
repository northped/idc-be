﻿using System.Collections.Generic;

namespace InduCompDewata.Application.Models.Generics
{
    public class DataTableModel<TTable> where TTable : class
    {
        public PaginationModel Pagination { get; set; }

        public IEnumerable<TTable> Items { get; set; }
    }

    public class PaginationModel
    {
        public bool Descending { get; set; }

        public int Page { get; set; }

        public int RowsPerPage { get; set; }

        public string SortBy { get; set; }

        public string Search { get; set; }

        public int TotalItems { get; set; }

        public IList<FilterModel> Filters { get; set; }
    }

    public class FilterModel
    {
        public string Column { get; set; }

        public IList<string> Values { get; set; }
    }
}
