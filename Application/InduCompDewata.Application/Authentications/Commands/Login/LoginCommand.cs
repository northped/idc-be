using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using InduCompDewata.Application.Interfaces;
using MediatR;

namespace InduCompDewata.Application.Authentications.Commands.Login
{
    public class LoginCommand : IRequest<string>
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public class Handler : IRequestHandler<LoginCommand, string>
        {
            private IAuthenticationService _authenticationService;

            public Handler(IAuthenticationService authenticationService)
            {
                _authenticationService = authenticationService;
            }

            public async Task<string> Handle(LoginCommand request, CancellationToken cancellationToken)
            {
                var loginToken = await _authenticationService.GetLoginToken(request.Username, request.Password);

                return loginToken;
            }
        }
    }
}
