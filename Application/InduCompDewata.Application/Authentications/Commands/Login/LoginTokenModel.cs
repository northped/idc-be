using System;
namespace InduCompDewata.Application.Authentications.Commands.Login
{
    public class LoginTokenModel
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
