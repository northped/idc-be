using System;
using System.Threading.Tasks;

namespace InduCompDewata.Application.Interfaces
{
    public interface IAuthenticationService
    {
        Task<string> GetLoginToken(string username, string password);
    }
}
