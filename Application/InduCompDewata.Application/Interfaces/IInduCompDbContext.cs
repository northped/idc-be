using System.Threading;
using System.Threading.Tasks;
using InduCompDewata.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace InduCompDewata.Application.Interfaces
{
    public interface IInduCompDbContext
    {
        DbSet<Employee> Employees { get; set; }

        DbSet<AsyncTask> AsyncTasks { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
