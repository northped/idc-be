﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using InduCompDewata.Domain.Entities;

namespace InduCompDewata.Application.Interfaces
{
    public interface IExcelFile<T> where T : class
    {
        Task<IEnumerable<T>> ReadAsync(MemoryStream stream, AsyncTask process);

        Task<MemoryStream> WriteAsync(IEnumerable<T> data, AsyncTask process);
    }
}
