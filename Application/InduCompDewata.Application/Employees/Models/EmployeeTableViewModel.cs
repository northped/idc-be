﻿using System;
using System.Linq.Expressions;
using InduCompDewata.Domain.Entities;

namespace InduCompDewata.Application.Employees.Models
{
    public class EmployeeTableViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string EmployeeId { get; set; }

        public string Title { get; set; }

        public string Department { get; set; }

        public string Customer { get; set; }

        public string SubCustomer { get; set; }

        public string Sex { get; set; }

        public string Status { get; set; }

        public static Expression<Func<Employee, EmployeeTableViewModel>> Projection
        {
            get
            {
                return e => new EmployeeTableViewModel
                {
                    Id = e.Id,
                    Name = e.Name,
                    EmployeeId = e.EmployeeId,
                    Title = e.Title,
                    Department = e.Department,
                    Customer = e.Customer,
                    SubCustomer = e.SubCustomer,
                    Sex = e.Sex,
                    Status = e.LastDate.HasValue ? "Non-Aktif" : "Aktif"
                };
            }
        }
    }
}
