﻿using System.Linq;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using InduCompDewata.Application.Employees.Models;
using InduCompDewata.Application.Interfaces;
using InduCompDewata.Domain.Entities;
using MediatR;

namespace InduCompDewata.Application.Employees.Commands
{
    public class ImportEmployeeCommand : IRequest
    {
        public MemoryStream EmployeeFile { get; set; }
    }

    public class ImportEmployeeCommandHandler : IRequestHandler<ImportEmployeeCommand, Unit>
    {
        private readonly IInduCompDbContext _dbContext;
        private readonly IExcelFile<Employee> _excelFile;

        public ImportEmployeeCommandHandler(IInduCompDbContext dbContext, IExcelFile<Employee> excelFile)
        {
            _dbContext = dbContext;
            _excelFile = excelFile;
        }

        public async Task<Unit> Handle(ImportEmployeeCommand request, CancellationToken cancellationToken)
        {
            AsyncTask task = new AsyncTask();
            var employees = await _excelFile.ReadAsync(request.EmployeeFile, task);
            if (employees.Any())
            {
                await _dbContext.Employees.AddRangeAsync(employees, cancellationToken);

                await _dbContext.SaveChangesAsync(cancellationToken);
            }

            var employee = _dbContext.Employees.Select(EmployeeTableViewModel.Projection).FirstOrDefault();

            return Unit.Value;
        }
    }
}
