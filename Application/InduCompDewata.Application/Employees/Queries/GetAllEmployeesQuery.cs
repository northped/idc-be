﻿using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using InduCompDewata.Application.Interfaces;
using InduCompDewata.Domain.Entities;
using MediatR;
using InduCompDewata.Application.Models.Generics;
using System.Collections.Generic;
using InduCompDewata.Application.Employees.Models;

namespace InduCompDewata.Application.Employees.Queries
{
    public class GetAllEmployeesQuery: PaginationModel, IRequest<DataTableModel<EmployeeTableViewModel>>
    {
        public string FiltersString { get; set; }
    }

    public class GetAllEmployeesQueryHandler : IRequestHandler<GetAllEmployeesQuery, DataTableModel<EmployeeTableViewModel>>
    {
        private readonly IInduCompDbContext _dbContext;
        private int iterator;
        private IList<string> values;

        public GetAllEmployeesQueryHandler(IInduCompDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<DataTableModel<EmployeeTableViewModel>> Handle(GetAllEmployeesQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Employee> employees = _dbContext.Employees;

            employees = FilterResult(employees, request.Filters);
            employees = SortResult(employees, request);

            return await PaginateResult(employees, request);
        }

        private IQueryable<Employee> FilterResult(IQueryable<Employee> employees, IEnumerable<FilterModel> filters)
        {
            if (filters == null || !filters.Any())
            {
                return employees;
            }

            iterator = 0;
            values = new List<string>();

            string filter = ConstructFilter(filters);
            if (string.IsNullOrEmpty(filter))
            {
                return employees;
            }

            return employees.Where(filter, values.ToArray());
        }

        private string ConstructFilter(IEnumerable<FilterModel> filters)
        {
            IEnumerable<string> conditions = filters.Select(ConstructFilter);

            return string.Join(" && ", conditions);
        }

        private string ConstructFilter(FilterModel filter)
        {
            IEnumerable<string> conditions = filter.Values.Select(x =>
            {
                values.Add(x);

                return $"{filter.Column} == @{iterator++}";
            });

            return $"({ string.Join(" || ", conditions) })";
        }

        private static IQueryable<Employee> SortResult(IQueryable<Employee> employees, GetAllEmployeesQuery request)
        {
            if (!string.IsNullOrEmpty(request.SortBy))
            {
                var orderBy = request.SortBy;

                if (request.Descending)
                {
                    orderBy = $"{orderBy} descending";
                }

                employees = employees.OrderBy(orderBy);
            }

            return employees;
        }

        private static async Task<DataTableModel<EmployeeTableViewModel>> PaginateResult(IQueryable<Employee> employees, PaginationModel pagination)
        {
            var result = new DataTableModel<EmployeeTableViewModel>
            {
                Pagination = pagination
            };

            var employeesAsync = employees.Select(EmployeeTableViewModel.Projection).ToAsyncEnumerable();
            result.Pagination.TotalItems = await employeesAsync.Count();

            if (pagination.Page < 1)
            {
                result.Items = await employeesAsync.ToList();
            }
            else
            {
                result.Items = await employeesAsync
                    .Skip((pagination.Page - 1) * pagination.RowsPerPage)
                    .Take(pagination.RowsPerPage)
                    .ToList();
            }

            return result;
        }
    }
}
