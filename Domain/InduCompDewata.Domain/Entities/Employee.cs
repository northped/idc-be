using System;

namespace InduCompDewata.Domain.Entities
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string EmployeeId { get; set; }

        public string Title { get; set; }

        public string Department { get; set; }

        public string Customer { get; set; }

        public string SubCustomer { get; set; }

        public DateTime? JoinDate { get; set; }

        public DateTime? LastDate { get; set; }

        public string BirthPlace { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Sex { get; set; }

        public string Religion { get; set; }

        public string HighestEducation { get; set; }

        public string MaritalStatus { get; set; }

        public int NumberOfChildren { get; set; }

        public string Address { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string NationalIdentification { get; set; }
    }
}
