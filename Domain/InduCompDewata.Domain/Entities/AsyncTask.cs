﻿using System;
namespace InduCompDewata.Domain.Entities
{
    public class AsyncTask
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public int CurrentIndex { get; set; }

        public int TotalIndex { get; set; }

        public DateTime Started { get; set; }

        public DateTime? Completed { get; set; }
    }
}
