﻿using InduCompDewata.Application.Interfaces;
using InduCompDewata.Domain.Entities;
using InduCompDewata.Infrastructure.File.Configurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InduCompDewata.Infrastructure.File
{
    public static class StartupExtensions
    {
        public static void AddInfrastructureFile(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IExcelFile<>), typeof(ExcelFileHelper<>));
            services.AddScoped<IEntityTypeConfiguration<Employee>, EmployeeTypeConfiguration>();
        }
    }
}
