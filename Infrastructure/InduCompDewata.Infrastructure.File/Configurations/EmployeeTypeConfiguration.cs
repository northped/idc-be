﻿using System;
using InduCompDewata.Domain.Entities;

namespace InduCompDewata.Infrastructure.File.Configurations
{
    public class EmployeeTypeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.SetSheetNumber(2)
                .SkipRows(8)
                .MapProperty(x => x.Name).Setting(1, "Nama")
                .MapProperty(x => x.EmployeeId).Setting(2, "ID No")
                .MapProperty(x => x.Title).Setting(3, "Title")
                .MapProperty(x => x.Department).Setting(4, "Department")
                .MapProperty(x => x.Customer).Setting(5, "Customer")
                .MapProperty(x => x.SubCustomer).Setting(6, "Bagian")
                .MapProperty(x => x.JoinDate).Setting(8, "Date Joint")
                .MapProperty(x => x.LastDate).Setting(10, "Last Working Day")
                .MapProperty(x => x.BirthPlace).Setting(12, "Tempat Lahir")
                .MapProperty(x => x.Sex).Setting(13, "Jenis Kelamin")
                .MapProperty(x => x.DateOfBirth).Setting(14, "Tanggal Lahir")
                .MapProperty(x => x.Religion).Setting(16, "Agama")
                .MapProperty(x => x.HighestEducation).Setting(17, "Pendidikan Terakhir")
                .MapProperty(x => x.MaritalStatus).Setting(18, "Status")
                .MapProperty(x => x.NumberOfChildren).Setting(19, "Anak")
                .MapProperty(x => x.Address).Setting(20, "Alamat")
                .MapProperty(x => x.PhoneNumber1).Setting(21, "No.Telepon 1")
                .MapProperty(x => x.PhoneNumber2).Setting(22, "No.Telepon 2")
                .MapProperty(x => x.NationalIdentification).Setting(23, "No. KTP");
        }
    }
}
