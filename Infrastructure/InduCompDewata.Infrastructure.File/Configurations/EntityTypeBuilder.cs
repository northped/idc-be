﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using NPOI.SS.UserModel;

namespace InduCompDewata.Infrastructure.File.Configurations
{
    public class EntityTypeBuilder<TEntity> where TEntity : class, new()
    {
        private int _rowsSkip;
        private int _sheetToBeProcessed;
        private readonly IList<MemberExpression> _expressions;
        private readonly IDictionary<string, PropertyMap<TEntity>> _propertyMaps;

        public EntityTypeBuilder()
        {
            _expressions = new List<MemberExpression>();
            _propertyMaps = new Dictionary<string, PropertyMap<TEntity>>();
        }

        public EntityTypeBuilder<TEntity> SkipRows(int rowsSkip)
        {
            _rowsSkip = rowsSkip;

            return this;
        }

        public EntityTypeBuilder<TEntity> SetSheetNumber(int sheetNumber)
        {
            _sheetToBeProcessed = sheetNumber;

            return this;
        }

        public int SheetToBeProcessed
        {
            get
            {
                return _sheetToBeProcessed;
            }
        }

        public int RowsToSkip
        {
            get
            {
                return _rowsSkip;
            }
        }

        public PropertyMap<TEntity> MapProperty(Expression<Func<TEntity, DateTime?>> propertyExpression)
        {
            return MapProperty(propertyExpression.Body);
        }

        public PropertyMap<TEntity> MapProperty(Expression<Func<TEntity, string>> propertyExpression)
        {
            return MapProperty(propertyExpression.Body);
        }

        public PropertyMap<TEntity> MapProperty(Expression<Func<TEntity, int>> propertyExpression)
        {
            return MapProperty(propertyExpression.Body);
        }

        private PropertyMap<TEntity> MapProperty(Expression expression)
        {
            if (!(expression is MemberExpression body))
            {
                throw new ArgumentException("'expression' should be a member expression");
            }

            PropertyMap<TEntity> propertyMap;

            var propertyInfo = (PropertyInfo)body.Member;
            var propertyType = propertyInfo.PropertyType;
            var propertyName = propertyInfo.Name;

            var name = propertyInfo.Name;

            if (_propertyMaps.ContainsKey(name))
            {
                propertyMap = _propertyMaps[name];
            }
            else
            {
                propertyMap = new PropertyMap<TEntity> { Parent = this };
                _propertyMaps.Add(name, propertyMap);
                _expressions.Add(body);
            }

            return propertyMap;
        }

        public TEntity FromRow(IRow row)
        {
            var entity = new TEntity();

            foreach(var expression in _expressions)
            {
                var propertyInfo = (PropertyInfo)expression.Member;
                var propertyType = propertyInfo.PropertyType;
                var name = propertyInfo.Name;

                if (!_propertyMaps.ContainsKey(name))
                {
                    continue;
                }

                var map = _propertyMaps[name];
                var cell = row.GetCell(map.Column);

                if (cell != null)
                {
                    switch (cell.CellType)
                    {
                        case CellType.Blank:
                            break;
                        case CellType.Numeric:
                            if (propertyType == typeof(string))
                            {
                                SetStringValue(entity, expression, cell.ToString());
                            }
                            else
                            {
                                if (DateUtil.IsCellDateFormatted(cell))
                                {
                                    try
                                    {
                                        SetPropertyValue(entity, expression, cell.DateCellValue);
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        SetPropertyValue(entity, expression, Convert.ToInt32(cell.NumericCellValue));
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }
                            }

                            break;
                        default:
                            SetStringValue(entity, expression, cell.StringCellValue);

                            break;
                    }
                }
            }

            return entity;
        }

        private static void SetStringValue(TEntity entity, MemberExpression expression, string cellValue)
        {
            var value = cellValue.Trim();
            if (!string.IsNullOrEmpty(value) && value != "-")
            {
                try
                {
                    SetPropertyValue(entity, expression, value);
                }
                catch (Exception e)
                {
                }
            }
        }

        private static void SetPropertyValue<TValue>(TEntity target, MemberExpression expression, TValue value)
        {
            if (expression.Member is PropertyInfo property)
            {
                property.SetValue(target, value, null);
            }
        }
    }

    public class PropertyMap<TEntity> where TEntity : class, new()
    {
        internal int Column { get; set; }

        internal string Title { get; set; }

        internal EntityTypeBuilder<TEntity> Parent { get; set; }

        public EntityTypeBuilder<TEntity> Setting(int column, string title = "")
        {
            Column = column;
            Title = title;

            return Parent;
        }
    }
}
