﻿using System;
namespace InduCompDewata.Infrastructure.File.Configurations
{
    public interface IEntityTypeConfiguration<TEntity> where TEntity : class, new()
    {
        void Configure(EntityTypeBuilder<TEntity> builder);
    }
}
