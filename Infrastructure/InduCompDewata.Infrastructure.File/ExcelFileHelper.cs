﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using InduCompDewata.Application.Interfaces;
using InduCompDewata.Domain.Entities;
using InduCompDewata.Infrastructure.File.Configurations;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace InduCompDewata.Infrastructure.File
{
    public class ExcelFileHelper<T> : IExcelFile<T> where T: class, new()
    {
        private readonly EntityTypeBuilder<T> _entityTypeBuilder;

        public ExcelFileHelper(IEntityTypeConfiguration<T> configuration)
        {
            _entityTypeBuilder = new EntityTypeBuilder<T>();
            configuration.Configure(_entityTypeBuilder);
        }

        public async Task<IEnumerable<T>> ReadAsync(MemoryStream stream, AsyncTask process)
        {
            process.Status = "Reading File";

            stream.Position = 0;

            ISheet sheet;
            var data = new List<T>();
            try
            {
                process.Status = "Importing Data";

                var hssfwb = new XSSFWorkbook(stream);

                sheet = hssfwb.GetSheetAt(_entityTypeBuilder.SheetToBeProcessed);

                var headerRow = sheet.GetRow(_entityTypeBuilder.RowsToSkip);
                var cellCount = headerRow.LastCellNum;

                process.TotalIndex = sheet.LastRowNum;
                process.CurrentIndex = _entityTypeBuilder.RowsToSkip;

                for (int i = (sheet.FirstRowNum + _entityTypeBuilder.RowsToSkip + 1); i <= sheet.LastRowNum; i++)
                {
                    var row = sheet.GetRow(i);
                    process.CurrentIndex = i;

                    if (row == null) continue;

                    if (row.Cells.All(d => d.CellType == CellType.Blank) ||
                        row.GetCell(0) == null || string.IsNullOrEmpty(row.GetCell(0).ToString()) ||
                        row.GetCell(1) == null || string.IsNullOrEmpty(row.GetCell(1).ToString())) continue;

                    T entity = _entityTypeBuilder.FromRow(row);
                    data.Add(entity);
                }

                process.Status = "Completed";
                process.Completed = DateTime.Now;
            }
            catch (Exception e)
            {
                process.Status = $"Failed: {e}";
            }

            stream.Dispose();

            return data;
        }

        public Task<MemoryStream> WriteAsync(IEnumerable<T> data, AsyncTask process)
        {
            throw new System.NotImplementedException();
        }
    }
}
