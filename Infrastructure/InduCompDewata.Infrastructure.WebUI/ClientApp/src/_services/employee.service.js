import { authHeader } from '../_helpers';
const apiUrl = 'http://localhost:5000/api';

export const employeeService = {
    getAll
};

function getAll(pagination) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    let handleResponse = await fetch('${apiUrl}/employee?page=1&rowsperpage=10&sortby=name&descending=true', requestOptions);
    return handleResponse(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}