import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Leave from './views/Leave.vue';
import Help from './views/Help.vue';
import Assessment from './views/Assessment.vue';
import Attendance from './views/Attendance.vue';
import Salary from './views/Salary.vue';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/leave',
      name: 'leave',
      component: Leave,
    },
    {
      path: '/help',
      name: 'help',
      component: Help,
    },
    {
      path: '/assessment',
      name: 'assessment',
      component: Assessment,
    },
    {
      path: '/attendance',
      name: 'attendance',
      component: Attendance,
    },
    {
      path: '/salary',
      name: 'salary',
      component: Salary,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  next();
});
