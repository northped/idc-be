import { userService } from '../_services';
import { router } from '../router';
import { stat } from 'fs';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: {}, user: null };

export const employees = {
  namespaced: true,
  state: {
    pagination: {
      descending: false,
      page: 1,
      rowsPerPage: 10,
      sortBy: 'Name',
      totalItems: 0,
      rowsPerPageItems: [10, 15, 25]
    },
    items: [],
    loading: false
  },
  actions: {
    queryItems(context) {

      return new Promise((resolve, reject) => {

        const { sortBy, descending, page, rowsPerPage } = context.state.pagination

        userService.getAll()
          .then(
            (data) => {
              context.commit('_setItems', { items: data.items, totalItems: data.pagination.totalItems })
            },
            error => {
              context.dispatch('alert/error', error, { root: true });
            }
          );

        resolve()
      })
    }
  },
  mutations: {
    setPagination(state, payload) {
      state.pagination = payload
    },
    _setItems(state, { items, totalItems }) {
      state.items = items;
      state.pagination.totalItems = totalItems;
    }
  },
  getters: {
    loading(state) {
      return state.loading
    },
    pagination(state) {
      return state.pagination
    },
    items(state) {
      return state.items
    }
  }
}
