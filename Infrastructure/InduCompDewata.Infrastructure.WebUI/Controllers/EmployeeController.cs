﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using InduCompDewata.Application.Employees.Commands;
using InduCompDewata.Application.Employees.Queries;
using InduCompDewata.Application.Models.Generics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace InduCompDewata.Infrastructure.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class EmployeeController : BaseController
    {
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetAllEmployee([FromQuery] GetAllEmployeesQuery query)
        {
            if (!string.IsNullOrEmpty(query.FiltersString))
            {
                query.Filters = JsonConvert.DeserializeObject<IList<FilterModel>>(HttpUtility.UrlDecode(query.FiltersString));
            }

            var result = await Mediator.Send(query);

            return Json(result);
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Import(IFormFileCollection files)
        {
            var file = files[0];
            var command = new ImportEmployeeCommand { EmployeeFile = new MemoryStream() };

            file.CopyTo(command.EmployeeFile);

            try
            {
                var result = await Mediator.Send(command);

                return Ok();
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}
