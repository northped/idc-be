﻿using System;
using System.Threading.Tasks;
using InduCompDewata.Application.Authentications.Commands.Login;
using Microsoft.AspNetCore.Mvc;

namespace InduCompDewata.Infrastructure.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : BaseController
    {
        public UsersController()
        {
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Authenticate(LoginCommand command)
        {
            try
            {
                var response = await Mediator.Send(command);

                return Json(new { Token = response });
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }
    }
}
