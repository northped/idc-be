﻿using InduCompDewata.Application.Interfaces;
using InduCompDewata.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace InduCompDewata.Infrastructure.Data
{
    public class InduCompDbContext : DbContext, IInduCompDbContext
    {
        public InduCompDbContext(DbContextOptions<InduCompDbContext> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<AsyncTask> AsyncTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(InduCompDbContext).Assembly);
        }
    }
}
