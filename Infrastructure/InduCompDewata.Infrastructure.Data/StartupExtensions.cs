﻿using InduCompDewata.Application.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InduCompDewata.Infrastructure.Data
{
    public static class StartupExtensions
    {
        private const string ConnectionStringName = "InduCompDatabase";

        public static void AddInfrastructureData(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<IInduCompDbContext, InduCompDbContext>(GetDefaultOptions(configuration));
        }

        public static void AddInfraStructureDataDbContext<T>(this IServiceCollection services, IConfiguration configuration) where T : DbContext
        {
            services.AddDbContext<T>(GetDefaultOptions(configuration));
        }

        private static System.Action<DbContextOptionsBuilder> GetDefaultOptions(IConfiguration configuration)
        {
            return options => options.UseMySql(configuration.GetConnectionString(ConnectionStringName), b=> b.MigrationsAssembly("InduCompDewata.Infrastructure.WebUI"));
        }

        public static void UseInfrastructureData(this IApplicationBuilder app, IHostingEnvironment env, IConfiguration configuration)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<IInduCompDbContext>() as InduCompDbContext;

                context.Database.Migrate();
            }
        }
    }
}
