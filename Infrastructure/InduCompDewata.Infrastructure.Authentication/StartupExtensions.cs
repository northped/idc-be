﻿using System.Text;
using InduCompDewata.Application.Interfaces;
using InduCompDewata.Infrastructure.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace InduCompDewata.Infrastructure.Authentication
{
    public static class StartupExtensions
    {
        public static void AddInfrastructureAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddInfraStructureDataDbContext<InduCompDewataIdentityDbContext>(configuration);

            services.AddIdentity<IdentityUser, IdentityRole>(
                     option =>
                     {
                         option.Password.RequireDigit = false;
                         option.Password.RequiredLength = 6;
                         option.Password.RequireNonAlphanumeric = false;
                         option.Password.RequireUppercase = false;
                         option.Password.RequireLowercase = false;
                     }
                 ).AddEntityFrameworkStores<InduCompDewataIdentityDbContext>()
                 .AddDefaultTokenProviders();

            services.AddAuthentication(option => {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options => {
                options.SaveToken = true;
                options.RequireHttpsMetadata = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = configuration["Jwt:Site"],
                    ValidIssuer = configuration["Jwt:Site"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SigningKey"]))
                };
            });

            services.AddTransient<IAuthenticationService, AuthenticationService>();
        }

        public static void UseInfrastructureAuthentication(this IApplicationBuilder app, IHostingEnvironment env, IConfiguration configuration)
        {
            app.UseAuthentication();
        }
    }
}
