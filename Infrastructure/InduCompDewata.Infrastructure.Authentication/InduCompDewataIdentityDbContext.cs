﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace InduCompDewata.Infrastructure.Authentication
{
    public class InduCompDewataIdentityDbContext : IdentityDbContext
    {
        public InduCompDewataIdentityDbContext(DbContextOptions<InduCompDewataIdentityDbContext> options)
            : base(options)
        {
        }
    }
}
